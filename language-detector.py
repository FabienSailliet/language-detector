#!/usr/bin/env python3

import argparse
import json
import sys
from typing import Set, Dict, IO, List


VERSION = "1.0.0"


def get_args():
    parser = argparse.ArgumentParser(description="Detect the language in which a text is written.")

    subparsers = parser.add_subparsers(
        title="command",
        help="Action to perform",
        dest="command"
    )
    subparsers.required = True

    # Train

    train_parser = subparsers.add_parser(
        "train",
        description="Train a database with a given text"
    )

    train_parser.add_argument(
        "language",
        help="language key of the given text (ex: 'en', 'fr', ...)"
    )

    train_parser.add_argument(
        "-i", "--input",
        help="file to read the text from: if not used, read from stdin."
    )

    train_parser.add_argument(
        "-d", "--database",
        help="file to write the database into, can be an existing one to complete or a new one to create: if not "
             "used, write to stdout "
    )

    # Detection

    detect_parser = subparsers.add_parser(
        "detect",
        description="Detect a text's language"
    )

    detect_parser.add_argument(
        "database",
        help="file to read the database from"
    )

    detect_parser.add_argument(
        "-i", "--input",
        help="file to read the text from: if not used, read from stdin."
    )

    return parser.parse_args()


def get_words(text: IO) -> Set[str]:
    words = set()
    word = ""

    for line in text:
        for char in line:
            lowered_char = char.lower()
            if lowered_char.isalpha():
                word += lowered_char
            else:
                if word != "":
                    words.add(word)
                    word = ""

    return words


def train(input_file_path: str = None, input_language: str = None, database_file_path: str = None) -> None:
    # Get input text words, from file or stdin
    if input_file_path is not None:
        with open(input_file_path) as f:
            words = get_words(f)
    else:
        words = get_words(sys.stdin)

    # Get database from file, or create empty one
    database = {}
    if database_file_path is not None:
        try:
            with open(database_file_path) as f:
                database: Dict[str, List[str]] = json.loads(f.read())
        except FileNotFoundError as e:
            print("Cannot find database file '{}', use empty one".format(e.filename))

    # Register words in database
    for word in words:
        if word not in database:
            database[word] = [input_language]
        else:
            database[word].append(input_language)

    # Save database in file, or print it
    database_str = json.dumps(database, separators=(',', ':'))
    if database_file_path is not None:
        with open(database_file_path, "w") as f:
            f.write(database_str)
    else:
        print(database_str)


def detect(database_file_path: str, input_file_path: str = None) -> None:
    # Get database from file
    try:
        with open(database_file_path) as f:
            database: Dict[str, List[str]] = json.loads(f.read())
    except FileNotFoundError as e:
        print("Cannot find database file '{}'".format(e.filename), file=sys.stderr)
        return

    # Get input text words, from file or stdin
    if input_file_path is not None:
        with open(input_file_path) as f:
            words = get_words(f)
    else:
        words = get_words(sys.stdin)

    words_languages: Dict[str, int] = {}
    total_words_nb = len(words)
    unknown_language_words_nb = 0

    # Determine words languages
    for word in words:
        if word in database:
            for language in database[word]:
                if language not in words_languages:
                    words_languages[language] = 0

                words_languages[language] += 1
        else:
            unknown_language_words_nb += 1

    result = {
        language: words_nb / total_words_nb
        for language, words_nb in words_languages.items()
    }
    if unknown_language_words_nb > 0:
        result["unknown"] = unknown_language_words_nb / total_words_nb

    displayed_results = []
    for language, ratio in result.items():
        text = f"{language}: {round(ratio * 100, 1)}%"
        bar = "=" * int(ratio * 40)

        displayed_results.append((text, bar))

    left_col_width = max(len(result[0]) for result in displayed_results) + 5

    for result in displayed_results:
        print(f"{result[0]:<{left_col_width}}{result[1]}")


if __name__ == '__main__':
    args = get_args()

    if args.command == "train":
        train(args.input, args.language, args.database)
    if args.command == "detect":
        detect(args.database, args.input)
